import datetime
import requests
import sys
from bs4 import BeautifulSoup

from LiveTrades2 import API
from LiveTrades2.Containers.Route import Route
from LiveTrades2.Containers.Station import Station


def get_sell_stations(commodity, max_star_distance, max_price_age):

    # EDDB URL
    url = "https://eddb.io/commodity/%s" % commodity.eddb_id

    # Load page
    page = BeautifulSoup(requests.get(url).text, "html.parser")

    # Extract rows from page
    rows = page.find(id="table-stations-max-sell").tbody.find_all("tr")

    # Loop for every row in rows
    for row in rows:

        # Collect columns for the row
        col = row.find_all("td")

        # If largest pad size is Large
        if col[5].span.text == "L":

            # Get station name
            name = col[0].a.text
            # Get station system name
            system = col[1].a.text

            # Get station star distance and update time using EDSM API
            station = API.get_station_info(name, system)
            # Calculate price age by subtracting the station update time from the current time
            price_age = divmod((datetime.datetime.now() - station.update_time).total_seconds(), 3600)[0]

            # If the station is within the max star distance and has been updated in the allowed number of days
            if station.distance <= max_star_distance and price_age < (max_price_age * 24):

                # Extract commodity sell price
                price = int(col[2].span.text.replace(",", ""))
                # Extract commodity demand
                demand = int(col[4].span.text.replace(",", ""))

                # Create station with details collected and add it to the commodity stations list
                station.price = price
                station.quantity = demand
                commodity.stations.append(station)

        # Print commodity and number of stations found so far
        out = "Finding...    Commodity: %s    Stations Found: %d" % (commodity.name, len(commodity.stations))
        sys.stdout.write('\r' + out)

    # Print empty to move to next line
    print("")

    # Return commodity with stations inside
    return commodity


def get_routes(commodity, radius, max_star_distance, min_supply, max_price_age):

    # Empty array to store routes
    routes = []
    # Counter variable for progress updates
    station_counter = 0
    # For every station in the commodity stations
    for station in commodity.stations:
        # EDSM URL
        url = "https://www.edsm.net/en_GB/search/stations/index/buyCommodity/%s/cmdrPosition/%s/sortBy/distanceCMDR" \
              "/type/1/type/2/type/3/type/5/type/6/type/11/p/" % (commodity.edsm_id, station.system)
        # Load page
        page = load_page(url, 1)
        # Collect rows from current page
        rows = page.find(class_="table table-hover").tbody.find_all("tr")
        # Loop for every row in rows
        for row in rows:
            # Collect columns for the row
            col = row.find_all("td")
            # Extract the nearby station distances (remove spaces and split on newlines)
            distances = col[8].text.replace(" ", "").replace("", "").split("\n")
            # Check nearby station is within the radius, if not break the loop
            system_distance = float(distances[1].strip().replace("ly", "").replace(",", ""))
            if system_distance > radius:
                break
            # Get nearby station star distance (remove front and back spaces, remove ls and remove commas)
            star_distance = int(float(distances[4].strip().replace("ls", "").replace(",", "")))
            # Check nearby station star distance is within the defined maximum star distance
            if star_distance <= max_star_distance:
                # Extract supply
                supply = int(col[6].text.replace(" ", "").replace(",", "").replace("t", "").strip())
                # Check nearby station supply is more than the defined minimum supply
                if supply >= min_supply:
                    # Get station name
                    name = col[1].strong.text
                    # Get station system name
                    system = col[1].small.a.text
                    # Get station type and update time from API
                    nearby_station = API.get_station_info(name, system)
                    # Calculate price age by subtracting the station update time from the current time
                    price_age = divmod((datetime.datetime.now() - nearby_station.update_time).total_seconds(), 3600)[0]
                    # If station has been updated in the allowed number of days
                    if price_age < (max_price_age * 24):
                        # Extract buy price
                        price = int(col[5].find_all("span")[0].text.replace(" cr", "").replace(",", ""))
                        # Create Station object for nearby station
                        nearby_station.price = price
                        nearby_station.quantity = supply
                        # Create Route object
                        route = Route(nearby_station, station, system_distance, commodity.name)
                        # Add new route
                        routes.append(route)

        # Print Search Update
        station_counter = station_counter + 1
        out = "Searching...    Commodity: %s    Stations Checked: %d" % (commodity.name, station_counter)
        sys.stdout.write('\r' + out)

    # Print to move output to the next line
    print()

    # Return routes collected after sorting them by profit in descending order
    return sorted(routes, key=lambda x: x.profit, reverse=True)


def get_system_id(station, system):
    # Empty id variable
    id = 0
    # EDSM URL
    url = "https://www.edsm.net/en_GB/search/stations/index/name/%s/sortBy/distanceSol" % station
    # Load page
    page = load_page(url, 1)
    # Collect rows from current page
    rows = page.find(class_="table table-hover").tbody.find_all("tr")
    # Loop for every row in rows
    for row in rows:
        # Collect columns for the row
        col = row.find_all("td")
        # Check if row system is equal to the system we're looking for
        if col[1].small.a.text == system:
            id = col[1].small.a["href"].split('/')[4]
    return id


def load_page(url, page_number):
    return BeautifulSoup(requests.get(url + str(page_number)).text, "html.parser")
