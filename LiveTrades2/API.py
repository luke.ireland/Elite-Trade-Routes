import json
import requests
import datetime
import time

from bs4 import BeautifulSoup

from LiveTrades2 import Scraper
from LiveTrades2.Containers.System import System
from LiveTrades2.Containers.Station import Station


# Fetch all systems within a radius of a center system
def get_systems(center_system, radius):
    #time.sleep(2)
    response = requests.get("https://www.edsm.net/api-v1/sphere-systems?systemName=%s&radius=%d"
                                       "&showInformation=1" % (center_system, radius))
    data = json.loads(response.text)
    print(response.headers["x-rate-limit-remaining"])
    systems = []
    print(center_system + ", " + str(radius) + ", " + str(response))
    for nearby_system in data:
        # Check if economy values are present, allocate accordingly
        economy = None if "economy" not in nearby_system["information"]\
            else nearby_system["information"]["economy"]
        second_economy = None if "secondEconomy" not in nearby_system["information"]\
            else nearby_system["information"]["secondEconomy"]

        systems.append(System(nearby_system["name"], nearby_system["distance"], economy=economy,
                              second_economy=second_economy))

    return systems


# Fetch all stations from a system
def get_stations(system):
    #time.sleep(2)
    response = requests.get("https://www.edsm.net/api-system-v1/stations?systemName=%s" % system)
    data = json.loads(response.text)
    print(response.headers["x-rate-limit-remaining"])
    stations = []
    print(system + ", " + str(data))
    for station in data["stations"]:
        stations.append(Station(station["name"],
                                system,
                                type=station["type"],
                                distance=station["distanceToArrival"],
                                update_time=station["updateTime"]["market"]))

    return stations


# Fetch commodity supply and buy price from a station
def get_commodity(station, system, commodity):
    #time.sleep(2)
    response = requests.get("https://www.edsm.net/api-system-v1/stations/market?systemName=%s"
                                       "&stationName=%s" % (system, station))
    data = json.loads(response.text)
    print(response.headers["x-rate-limit-remaining"])
    supply = 0
    price = 0
    print(station + ", " + system + ", " + str(data))
    for c in data["commodities"]:
        if c["id"] == commodity:
            supply = c["stock"]
            price = c["buyPrice"]

    return [supply, price]


# Fetch star distance and latest update time for a station in a system
def get_station_info(station, system):
    response = requests.get("https://www.edsm.net/api-system-v1/stations?systemName=%s" % system)
    data = json.loads(response.text)
    # Check for empty data, possible duplicate
    if not bool(data):
        id = Scraper.get_system_id(station, system)
        response = requests.get("https://www.edsm.net/api-system-v1/stations?systemId=%s" % id)
        data = json.loads(response.text)
    type = "None"
    distance = 9999
    update_time = datetime.datetime.strptime("1970-01-01 00:00:00", "%Y-%m-%d %H:%M:%S")
    for system_station in data["stations"]:
        if system_station["name"].replace(" ", "") == station.replace(" ", ""):
            type = system_station["type"].split(" ")[0]
            distance = system_station["distanceToArrival"]
            update_time = datetime.datetime.strptime(system_station["updateTime"]["market"], "%Y-%m-%d %H:%M:%S")

    return Station(station, system, type=type, distance=int(float(distance)), update_time=update_time)


def load_page(url, page_number):
    return BeautifulSoup(requests.get(url + str(page_number)).text, "html.parser")