from datetime import datetime
from LiveTrades2.Containers.Commodity import Commodity
from LiveTrades2 import Scraper, Export


def int_input(question):
    answer = input(question)
    while not answer.isdigit():
        print("Input was NOT an INTEGER. Please try again.")
        answer = input(question)
    return int(answer)


# Create commodity dictionary to store all commodity details
possible_commodities = {
    "AT": Commodity("Agronomic Treatment", "360", "116"),
    "B": Commodity("Bauxite", "51", "901"),
    "G": Commodity("Gold", "42", "806"),
    "P": Commodity("Palladium", "45", "809"),
    "S": Commodity("Silver", "47", "811"),
    "CC": Commodity("Ceramic Composites", "115", "505"),
    "MA": Commodity("Meta-Alloys", "114", "504"),
    "T": Commodity("Tritium", "362", "117")
}

# Maximum distance from Sol to a system allowed
max_sol_distance = 1000

# Commodities
commodities = []
commodity_names = []





# Look into adding reference system parameter








# Main Title
print("")
print("---------------------- LIVE TRADES 2.0 ----------------------")

# Opening statement for commodity selection
print("Enter the SYMBOL of each COMMODITY you want to SEARCH for.")

# List of all possible commodities and their symbols
for symbol, possible_commodity in possible_commodities.items():
    print("%s = %s" % (possible_commodity.name, symbol))

# User Input for commodity selection
commodity_string = input("Commodities (e.g. G,AT): ")

# For every symbol entered in the commodity selection
for symbol in commodity_string.split(","):
    # If the symbol entered matches a possible commodity
    if symbol in possible_commodities:
        # Add the possible commodity as a search commodity
        commodities.append(possible_commodities[symbol])
        # Add the name of the new commodity to an array for html export
        commodity_names.append(possible_commodities[symbol].name)


# Search Parameters Title
print("\n--------------------- SEARCH PARAMETERS ---------------------")

# User Input for maximum range
max_range = int_input("Maximum range (ly): ")
# User Input for maximum star distance
max_star_distance = int_input("Maximum star distance (ls): ")
# User Input for minimum supply
min_supply = int_input("Minimum supply (t): ")
# User Input for maximum price age
max_price_age = int_input("Maximum price age (days): ")


# Save Search Parameters and Timestamp for HTML Export
params = [max_range, max_star_distance, min_supply, max_price_age]
now = datetime.now()
timestamp = [now.strftime("%H:%M"), now.strftime("%d %B %Y")]

# Finding Sell Stations Title
print("\n------------------- FINDING SELL STATIONS -------------------")

# For every commodity, find high price sell stations
for i in range(len(commodities)):
    commodities[i] = Scraper.get_sell_stations(commodities[i], max_star_distance, max_price_age)


# Searching For Routes Title
print("\n-------------------- SEARCHING FOR ROUTES -------------------")

# For every commodity
for commodity in commodities:
    # Use the scraper to gather routes for the commodity based on the defined parameters
    commodity.routes = Scraper.get_routes(commodity, max_range, max_star_distance, min_supply,
                                                        max_price_age)


# Exporting Title
print("\n------------------------- EXPORTING -------------------------")

routes = []
for commodity in commodities:
    routes = routes + commodity.routes
routes = sorted(routes, key=lambda x: x.profit, reverse=True)

Export.to_html(params, commodity_names, timestamp, routes)

# Routes Title
print("\n-------------------------- ROUTES ---------------------------")

# For every active commodity
for commodity in commodities:

    # Print current commodity
    print("\n" + commodity.name + ":")

    # For every route in commodity routes
    for route in commodity.routes:
        print(route)
