import datetime
import shutil

from yattag import Doc, indent

from LiveTrades2.Containers.Route import Route


def generate_settings(doc, tag, text, params, commodity_names, timestamp):
    with tag("div", klass="settings"):
        with tag("div", klass="parameters"):
            with tag("h2"):
                text("Search Parameters")
            with tag("table"):
                with tag("tr"):
                    with tag("td"):
                        text("Max. range (ly): ")
                    with tag("td"):
                        text(params[0])
                with tag("tr"):
                    with tag("td"):
                        text("Max. star dist. (ls): ")
                    with tag("td"):
                        text(params[1])
                with tag("tr"):
                    with tag("td"):
                        text("Min. supply (t): ")
                    with tag("td"):
                        text(params[2])
                with tag("tr"):
                    with tag("td"):
                        text("Max. price age (days): ")
                    with tag("td"):
                        text(params[3])
        with tag("div", klass="commodities"):
            with tag("h2"):
                text("Commodities")
            for i in range(len(commodity_names)):
                with tag("p"):
                    text(commodity_names[i])
        with tag("div", klass="time"):
            with tag("h2"):
                text("Time Generated")
            with tag("p"):
                text(timestamp[0])
            with tag("p"):
                text(timestamp[1])


def generate_card(doc, tag, text, route: Route):
    # Updated ago times
    buy_updated_tuple = divmod((datetime.datetime.now() - route.buy_station.update_time).total_seconds(), 3600)
    buy_updated = [int(buy_updated_tuple[0]), int(buy_updated_tuple[1] / 60)]
    sell_updated_tuple = divmod((datetime.datetime.now() - route.sell_station.update_time).total_seconds(), 3600)
    sell_updated = [int(sell_updated_tuple[0]), int(sell_updated_tuple[1] / 60)]

    # Specific tonnage profits
    dylprofit = route.profit * 728
    dylanprofit = route.profit * 720

    with tag("div", klass="card"):
        with tag("div", klass="tab"):
            with tag("p", klass="left"):
                text(route.commodity)
            with tag("p", klass="middle"):
                text(str(route.distance) + " ly")
            with tag("p", klass="right"):
                text("50 ly from Sol")
        with tag("div", klass="info"):
            with tag("div", klass="buy left"):
                with tag("p", klass="p-large"):
                    text(route.buy_station.name)
                with tag("p", klass="system"):
                    text(route.buy_station.system)
                with tag("table"):
                    with tag("tr"):
                        with tag("td"):
                            text("Dist. (ls): ")
                        with tag("td"):
                            text(f"{route.buy_station.distance:,d}")
                    with tag("tr"):
                        with tag("td"):
                            text("Supply (t): ")
                        with tag("td"):
                            text(f"{route.buy_station.quantity:,d}")
            with tag("div", klass="profit middle"):
                doc.stag("img", src="arrow.png")
                with tag("p"):
                    text(f"{route.profit:,d}" + " cr/t")
            with tag("div", klass="sell right"):
                with tag("p", klass="p-large"):
                    text(route.sell_station.name)
                with tag("p", klass="system"):
                    text(route.sell_station.system)
                with tag("table"):
                    with tag("tr"):
                        with tag("td"):
                            text("Dist. (ls): ")
                        with tag("td"):
                            text(f"{route.sell_station.distance:,d}")
                    with tag("tr"):
                        with tag("td"):
                            text("Demand (t): ")
                        with tag("td"):
                            text(f"{route.sell_station.quantity:,d}")
        with tag("div", klass="moreinfo"):
            with tag("div", klass="buy left"):
                with tag("table"):
                    with tag("tr"):
                        with tag("td"):
                            text("Type: ")
                        with tag("td"):
                            text(route.buy_station.type)
                    with tag("tr"):
                        with tag("td"):
                            text("Price (cr): ")
                        with tag("td"):
                            text(f"{route.buy_station.price:,d}")
                with tag("p", klass="p-small"):
                    text(f"Updated {buy_updated[0]}h {buy_updated[1]}m ago")
            with tag("div", klass="profit middle"):
                with tag("p"):
                    text(f"{dylprofit:,d} cr @ 728 t")
                with tag("p"):
                    text(f"{dylanprofit:,d} cr @ 720 t")
            with tag("div", klass="sell right"):
                with tag("table"):
                    with tag("tr"):
                        with tag("td"):
                            text("Type: ")
                        with tag("td"):
                            text(route.sell_station.type)
                    with tag("tr"):
                        with tag("td"):
                            text("Price (cr): ")
                        with tag("td"):
                            text(f"{route.sell_station.price:,d}")
                with tag("p", klass="p-small"):
                    text(f"Updated {sell_updated[0]}h {sell_updated[1]}m ago")
        with tag("div", klass="collapse"):
            doc.stag("img", src="chevron.png")

    return doc


def to_html(params, commodity_names, timestamp, routes):
    # Path of html file we will write to
    filename = "./Web/index.html"
    # Init yattag objects
    doc, tag, text = Doc().tagtext()

    # Open html file for writing
    f = open(filename, "w")

    # Start html
    with tag("html", lang="en"):
        with tag("head"):
            with tag("title"):
                text("Live Trades 2.0")
            doc.stag("link", rel="stylesheet", type="text/css", href="stylesheet.css")
            with tag("script",
                     src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js",
                     type='text/javascript'):
                pass
            with tag("script",
                     src="script.js",
                     type='text/javascript'):
                pass
        with tag("body"):

            # Header
            with tag("header"):
                with tag("h1"):
                    text("LIVE TRADES 2.0")

            # Settings with Search Parameters, Commodities and Timestamp
            generate_settings(doc, tag, text, params, commodity_names, timestamp)

            # Route Cards
            for route in routes:
                doc = generate_card(doc, tag, text, route)

    # Apply doc indentation
    indented_doc = indent(doc.getvalue(), indentation="    ")
    # Write doc to html file
    f.write(indented_doc)
    # Close html file
    f.close()
    # Copy file to main directory
    #shutil.copyfile(filename, "../index.html")
