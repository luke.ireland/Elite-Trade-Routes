class Station:

    def __init__(self, name, system, type=None, economy=None, price=None, quantity=None, distance=None,
                 update_time=None):
        self.name = name
        self.system = system
        self.type = None if type is None else type
        self.economy = None if economy is None else economy
        self.price = 0 if price is None else price
        self.quantity = 0 if quantity is None else quantity
        self.distance = 0 if distance is None else distance
        self.update_time = None if update_time is None else update_time

    def __str__(self):
        return "\nStation -" + \
               "\tName: " + self.name + \
               "\tSystem: " + self.system + \
               "\tType: " + str(self.type) + \
               "\tEconomy: " + str(self.economy) + \
               "\tPrice: " + str(self.price) + \
               "\tQuantity: " + str(self.quantity) + \
               "\tDistance: " + str(self.distance) + \
               "\tUpdate Time: " + str(self.update_time)
