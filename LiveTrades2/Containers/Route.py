class Route:

    def __init__(self, buy_station, sell_station, distance, commodity):
        self.buy_station = buy_station
        self.sell_station = sell_station
        self.distance = distance
        self.profit = sell_station.price - buy_station.price
        self.commodity = commodity

    def __str__(self):
        return "Route -    %s, %s  ------%s----->  %s, %s    Profit: %d" % (self.buy_station.name,
                                                                            self.buy_station.system, str(self.distance),
                                                                            self.sell_station.name,
                                                                            self.sell_station.system, self.profit)
