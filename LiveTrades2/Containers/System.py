class System:

    def __init__(self, name, distance, economy=None, second_economy=None):
        self.name = name
        self.distance = distance
        self.economy = None if economy is None else economy
        self.second_economy = None if second_economy is None else second_economy

    def __str__(self):
        return "\nSystem -" + \
               "\tName: " + self.name + \
               "\tDistance: " + str(self.distance) + \
               "\tEconomy: " + str(self.economy) + \
               "\tSecond Economy: " + str(self.second_economy)
