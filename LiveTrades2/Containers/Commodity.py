class Commodity:

    def __init__(self, name, eddb_id, edsm_id, stations=None, routes=None):
        self.name = name
        self.eddb_id = eddb_id
        self.edsm_id = edsm_id
        self.stations = [] if stations is None else stations
        self.routes = [] if routes is None else routes

    def __str__(self):
        return "\nCommodity -" + \
               "\tName: " + self.name + \
               "\tEDDB ID: " + self.eddb_id + \
               "\tEDSM ID: " + self.edsm_id + \
               "\tStation count: " + str(len(self.stations)) + \
               "\tRoute count: " + str(len(self.routes))
