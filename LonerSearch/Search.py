import json


def get_names(json):
    names = []
    for system in json:
        names.append(system["name"])
    return names


def print_results(results):
    for result in results:
        print(result)


def one_and_one(center_json, nearby_names):
    pairs = []
    for center in center_json:
        nearby_count = len(center["nearby"])
        if nearby_count == 1:
            nearby_system = center["nearby"][0]
            if nearby_system["nearby_name"] in nearby_names:
                pairs.append("Center: " + center["name"] + " | Nearby: " + nearby_system["nearby_name"])
    return pairs


def one_and_one_with_allegiance(center_json, nearby_names, center_allegiance):
    pairs = []
    for center in center_json:
        nearby_count = len(center["nearby"])
        if nearby_count == 1 and center["allegiance"] == center_allegiance:
            nearby_system = center["nearby"][0]
            if nearby_system["nearby_name"] in nearby_names:
                pairs.append("Center: " + center["name"] + " | Nearby: " + nearby_system["nearby_name"])
    return pairs


def one_includes_one(center_json, nearby_names, nearby_limit):
    pairs = []
    for center in center_json:
        nearby_count = len(center["nearby"])
        if nearby_count <= nearby_limit:
            for nearby in center["nearby"]:
                if nearby["nearby_name"] in nearby_names:
                    pairs.append("Center: " + center["name"] + " | Nearby: " + nearby["nearby_name"] + " | Nearby Count: " + str(nearby_count))

    return pairs


def one_includes_one_with_allegiance(center_json, nearby_names, nearby_limit, center_allegiance):
    pairs = []
    for center in center_json:
        nearby_count = len(center["nearby"])
        if nearby_count <= nearby_limit and center["allegiance"] == center_allegiance:
            for nearby in center["nearby"]:
                if nearby["nearby_name"] in nearby_names:
                    pairs.append("Center: " + center["name"] + " | Nearby: " + nearby["nearby_name"] + " | Nearby Count: " + str(nearby_count))

    return pairs


def main():
    # Load Loner and Duo JSON
    print("Loading Loner and Duo JSON")
    loners_json = json.load(open("resources/loners_radius20.json"))
    duos_json = json.load(open("resources/duos_radius20.json"))
    print("Loners Loaded: " + str(len(loners_json)))
    print("Duos Loaded: " + str(len(duos_json)))

    # Get Loner and Duo names
    loner_names = get_names(loners_json)
    duos_names = get_names(duos_json)

    # Search
    #results = one_and_one(loners_json, loner_names)
    #results = one_includes_one(loners_json, loner_names, 9)
    #results = one_and_one_with_allegiance(loners_json, loner_names, "Independent")
    results = one_includes_one_with_allegiance(loners_json, loner_names, 1, "Empire")

    # Print Results
    print_results(results)


main()