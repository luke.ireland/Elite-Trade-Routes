import requests
import gzip
import shutil

url = "https://www.edsm.net/dump/systemsPopulated.json.gz"
gz_location = "resources/systemsPopulated.json.gz"
json_location = "resources/systemsPopulated.json"

#Download .gz
print("Downloading from " + url)
with open("resources/systemsPopulated.json.gz", "wb") as f:
    r = requests.get(url)
    f.write(r.content)
print("Download Complete.")

#Unzip .gz
print("Unzipping .gz")
with gzip.open(gz_location, "rb") as f_in:
    with open(json_location, "wb") as f_out:
        shutil.copyfileobj(f_in, f_out)
print("Unzip Complete.")