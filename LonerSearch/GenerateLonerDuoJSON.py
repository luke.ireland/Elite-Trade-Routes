import json
import math
import sys


def calc_distance(coords1, coords2):
    return round(
        math.sqrt(
            math.pow(coords1["x"] - coords2["x"], 2) +
            math.pow(coords1["y"] - coords2["y"], 2) +
            math.pow(coords1["z"] - coords2["z"], 2))
        , 2)


def has_valid_medium_station(stations):
    valid = False
    for station in stations:
        if "Missions" in station["otherServices"]:
            valid = True
    return valid


def has_valid_large_station(stations):
    valid = False
    for station in stations:
        if ("Missions" in station["otherServices"] and
            station["type"] != "Outpost"):
            valid = True
    return valid


def count_factions(factions):
    faction_count = 0
    for faction in factions:
        if faction["influence"] != 0:
            faction_count = faction_count + 1
    return faction_count


def generate_system_dict(system, radius, populated_systems_json):
    system_dict = {"name": system["name"],
                   "allegiance": system["allegiance"]}
    nearby_list = []
    for populated_system in populated_systems_json:
        distance = calc_distance(system["coords"], populated_system["coords"])
        if distance < radius and distance != 0:
            nearby_list.append({"nearby_name": populated_system["name"],
                                "distance": distance})
    system_dict["nearby"] = nearby_list
    return system_dict


def main():
    # Load Populated Systems JSON
    print("Loading Populated Systems JSON")
    populated_systems_json = json.load(open("resources/systemsPopulated.json"))
    populated_count = len(populated_systems_json)
    print("Populated Systems Loaded: " + str(populated_count))

    # Get Loners and Duos from JSON
    radius = 20
    minimum_factions = 3
    system_count = 0
    loner_count = 0
    duo_count = 0
    loners = []
    duos = []

    print("Processing Loners and Duos")
    for system in populated_systems_json:
        # Increment system counter and write to console
        system_count += 1
        sys.stdout.flush()
        sys.stdout.write("\rSystem " + str(system_count) + " of " + str(populated_count) +
                         " | Loners: " + str(loner_count) + " | Duos: " + str(duo_count))

        # Filter out Loners and Duos
        stations_count = len(system["stations"])
        if stations_count == 1:
            if has_valid_medium_station(system["stations"]):
                if count_factions(system["factions"]) >= minimum_factions:
                    loners.append(generate_system_dict(system, radius, populated_systems_json))
                    loner_count += 1

        elif stations_count == 2:
            if has_valid_large_station(system["stations"]) and system["state"] == "Boom":
                if count_factions(system["factions"]) >= minimum_factions:
                    duos.append(generate_system_dict(system, radius, populated_systems_json))
                    duo_count += 1

    #Generate Loner and Duo JSON
    print("\nGenerating JSON files")

    with open("resources/loners_radius" + str(radius) + ".json", "w") as outfile:
        json.dump(loners, outfile)

    with open("resources/duos_radius" + str(radius) + ".json", "w") as outfile:
        json.dump(duos, outfile)


main()
