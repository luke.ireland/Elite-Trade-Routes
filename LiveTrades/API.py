import requests, json

from EDCoords import EDCoords
from EDRoute import EDRoute
from EDStation import EDStation


def get_station_routes(station: EDStation, buying_radius, current_coords, commodity, min_supply):
    response = json.loads(requests.get("https://www.edsm.net/api-v1/sphere-systems",
                            {"systemName": station.system_name,
                             "radius": buying_radius,
                             "showInformation": 1
                             }).content)

    data = []
    for system in response:
        current_to_buying = current_coords.distance_from(get_system_coords(system["name"]))
        for buying_station in get_buying_stations(system["name"], commodity, min_supply):
            data.append(EDRoute(buying_station, station, system["distance"], current_to_buying))

    return data

def get_buying_stations(system, commodity, min_supply):
    response = json.loads(requests.get("https://www.edsm.net/api-system-v1/stations",
                                       {"systemName": system
                                        }).content)

    data = []
    for station in response["stations"]:
        if (station["economy"] == "Refinery" or station["secondEconomy"] == "Refinery") and station["haveMarket"] is True:
            price, supply = get_buying_price_and_supply(system, station["name"], commodity)
            if supply > min_supply:
                data.append(EDStation(station["name"], price, supply, system))

    return data


def get_buying_price_and_supply(system, station, commodity):
    response = json.loads(requests.get("https://www.edsm.net/api-system-v1/stations/market",
                                       {"systemName": system,
                                        "stationName": station
                                        }).content)

    commodity_index = get_commodity_index(commodity, response["commodities"])
    commodity = response["commodities"][commodity_index]

    return commodity["buyPrice"], commodity["stock"]


def get_commodity_index(commodity, commodities):
    counter = 0
    for commodity in commodities:
        if commodity["id"] == commodity:
            return counter
        else:
            counter = counter + 1
    return 0

def get_system_coords(system):
    response = json.loads(requests.get("https://www.edsm.net/api-v1/system",
                                       {"systemName": system,
                                        "showCoordinates": 1
                                        }).content)
    return EDCoords(response["coords"]["x"], response["coords"]["y"], response["coords"]["z"])