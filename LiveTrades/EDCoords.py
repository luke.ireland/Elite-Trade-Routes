import math


class EDCoords:

    def __init__(self, x: float, y: float, z: float):
        self.x = x
        self.y = y
        self.z = z

    def distance_from(self, other):
        return round(
            math.sqrt(
            math.pow(self.x - other.x, 2) +
            math.pow(self.y - other.y, 2) +
            math.pow(self.z - other.z, 2))
            , 2)
