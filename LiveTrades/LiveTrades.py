from datetime import datetime
import Scraper
import API
import HTML

top_selling_stations = 10
buying_radius = 11
min_supply = 5000
commodity="gold"

# Get current system
current_system = input("Current System: ")
current_coords = API.get_system_coords(current_system)
capacity = input("Cargo capacity (Leave blank for default):")

# Get current date and time
dateandtime = datetime.now().strftime("%d/%m/%Y %H:%M:%S")

# Calculate routes
allRoutes = []
for selling_station in Scraper.get_selling_stations(top_selling_stations):
    for route in API.get_station_routes(selling_station, buying_radius, current_coords, commodity, min_supply):
        allRoutes.append(route)
        route.capacity = int(capacity)
        print(route)

allRoutes.sort(key=lambda x: x.profit, reverse=True)

# Turn routes into HTML table
HTML.generate(allRoutes, current_system, dateandtime)
