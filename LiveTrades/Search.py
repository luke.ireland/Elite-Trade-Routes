import Scraper
import API

commodity = "gold"
min_supply = 5000
ideal_min_supply = 10000
max_distance = 11


class Route:
    def __init__(self, buy_station, sell_station, profit):
        self.buy_station = buy_station
        self.sell_station = sell_station
        self.profit = profit

    def __str__(self):
        return "Buy: {} / Sell: {} / Profit: {}".format(
            self.buy_station, self.sell_station, self.profit
        )

def get_max_selling_stations(commodity, limit=10):
    # This should take a commodity and return the top X selling stations
    return Scraper.get_selling_stations(limit)


def get_stations_within_range(station, max_distance):
    # This should take a station and return all other stations within max_distance radius
    return


def get_stations_with_min_supply(stations, commodity, min_supply):
    # This should take a list of stations and return any with at least min_supply amount of commodity
    return


def get_sell_price(station, commodity):
    # This should return the sell price of commodity at station
    return


def get_buy_price(station, commodity):
    # This should return the sell price of commodity at station
    return


selling_stations = get_max_selling_stations(commodity)
routes = []
for station in selling_stations:
    buy_stations = get_stations_within_range(station, max_distance)
    valid_stations = get_stations_with_min_supply(
        buy_stations, commodity, ideal_min_supply)
    if not valid_stations:
        valid_stations = get_stations_with_min_supply(
            buy_stations, commodity, min_supply)
    sell_price = get_sell_price(station, commodity)
    for buy_station in valid_stations:
        buy_price = get_buy_price(buy_station, commodity)
        routes.append(Route(buy_station, station, sell_price-buy_price))

print(routes)
