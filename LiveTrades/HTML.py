from typing import List
from yattag import Doc
from EDRoute import EDRoute
from pathlib import Path

index = Path(__file__).parent / "Web" / "index.html"

def generate(allRoutes: List[EDRoute], current_system, dateandtime):
    doc, tag, text = Doc().tagtext()

    with tag('html', lang='en'):
        with tag('head'):
            with tag('title'):
                text("MGF Routes")
            doc.stag('link', rel='stylesheet', type='text/css',
                     href='stylesheet.css')
        with tag('body'):
            with tag('h1', klass='currentSystem'):
                text("Current System: " + current_system)
            with tag('h1', klass='dateAndTime'):
                text("Generated: " + dateandtime)
            with tag('table', klass='profits'):
                with tag('thead'):
                    with tag('tr'):
                        with tag('th'):
                            text("Buy Station")
                        with tag('th'):
                            text("Buy System")
                        with tag('th'):
                            text("Sell Station")
                        with tag('th'):
                            text("Sell System")
                        with tag('th'):
                            text("Sys. to Sys. (ly)")
                        with tag('th'):
                            text("Profit (cr/t)")
                        with tag('th'):
                            text("From you (ly)")
                with tag('tbody'):
                    for route in allRoutes:
                        with tag('tr'):
                            with tag('td'):
                                text(str(route.buying.station_name))
                            with tag('td'):
                                text(str(route.buying.system_name))
                            with tag('td'):
                                text(str(route.selling.station_name))
                            with tag('td'):
                                text(str(route.selling.system_name))
                            with tag('td'):
                                text(str(route.star_to_star))
                            with tag('td'):
                                text(str(route.profit))
                            with tag('td'):
                                text(str(route.current_to_buying))


    f = open(index, "w")
    f.write(doc.getvalue())
    f.close()