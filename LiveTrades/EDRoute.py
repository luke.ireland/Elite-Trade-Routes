from EDStation import EDStation

format_string = "{}, {} ---({} ly)---> {}, {} @ {} cr/t | {} ly from you{}"


class EDRoute:

    def __init__(self, buying: EDStation, selling: EDStation, star_to_star: float,
                 current_to_buying: float):
        self.buying = buying
        self.selling = selling
        self.star_to_star = star_to_star
        self.current_to_buying = current_to_buying
        self.profit = selling.price - buying.price
        self.capacity = 0

    def __str__(self):
        cap_string = ""
        if self.capacity > 0:
            cap_string = " | Total Profit: {}cr".format(
                self.profit*self.capacity)
        return format_string.format(
            self.buying.station_name, self.buying.system_name,
            self.star_to_star, self.selling.station_name,
            self.selling.system_name, self.profit,
            self.current_to_buying, cap_string
        )
